$(document).ready(function() {

    // tooltip

    var $tooltipEl = $('[data-toggle="tooltip"]');

    $tooltipEl.on('click', function () {
        return false;
    });
    $tooltipEl.tooltip({
        trigger: 'focus'
    });

});