var gulp = require("gulp");
var gulpif = require( 'gulp-if');
var jade = require("gulp-jade");
var stylus = require("gulp-stylus");
var poststylus = require('poststylus');
var autoprefixer = require('autoprefixer-stylus');
var stylint = require('gulp-stylint');
var minifyImg = require("gulp-imagemin");
var changed = require("gulp-changed");
var uglify = require("gulp-uglify");
var jslint = require('gulp-jslint');
var concat = require('gulp-concat');
var minifyCss = require("gulp-minify-css");
var browserSync = require('browser-sync');
var sourcemaps = require("gulp-sourcemaps");
var plumber = require("gulp-plumber");
var util = require("gulp-util");

var reload = browserSync.reload;

var error = function(e) {
    util.beep();
    util.log(e.toString());
};

var paths = {
    //public
    html: "public",
    css: "public/css",
    js: "public/js",
    image: "public/img",
    //develop
    jade: "assets/templates/pages/*.jade",
    img: "assets/img/**",
    stylus: "assets/stylus/*.styl"
};

gulp.task("browser-sync", function() {
    browserSync({
        server:{
            baseDir:"public"
        }
    });
    gulp.watch(paths.jade, ["html", reload]);
    gulp.watch(paths.stylus, ["css", reload]);
    gulp.watch("assets/js/**", ["js", reload]);
    gulp.watch(paths.img, ["img", reload]);
});

gulp.task("html", function() {
    return gulp.src(paths.jade)
        .pipe(plumber({"errorHandler":error}))
        .pipe(jade())
        .pipe(gulp.dest(paths.html));
});

gulp.task("css", function () {
    return gulp.src([paths.stylus])
        .pipe(plumber({"errorHandler":error}))
        .pipe(stylint())
        .pipe(gulpif("*.styl", stylus([poststylus([autoprefixer()])])))
        .pipe(minifyCss({compatibility: "ie7"}))
        .pipe(gulp.dest(paths.css));
});

gulp.task("js", function () {
    return gulp.src(['assets/js/libs/**/*.js', 'assets/js/scripts/*.js'])
        .pipe(plumber({"errorHandler":error}))
        .pipe(jslint())
        .pipe(concat('app.js'))
        .pipe(uglify())
        .pipe(gulp.dest(paths.js));
});

gulp.task("img", function() {
    return gulp.src(paths.img)
        .pipe(plumber({"errorHandler":error}))
        .pipe(minifyImg())
        .pipe(gulp.dest(paths.image));
});

gulp.task("default", function(){
    gulp.run("html", "css", "js", "img", "browser-sync");
});